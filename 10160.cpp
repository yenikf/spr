// 10160 - Servicing Stations

// we're going to create the adjacency matrix using an array of bitsets, then test it for logical OR

#include <iostream>
#include <bitset>
#include <array>
#include <algorithm>

using matrix = std::array<std::bitset<35>, 35>;

inline bool bitset_compare(std::bitset<35> const& lhs, std::bitset<35> const& rhs)
{
	if (lhs.count() > rhs.count() || (lhs.count() == rhs.count() && lhs.to_ullong() > rhs.to_ullong())) {
		return true;
	}
	return false;
}

matrix build_matrix(int cities, int roads)
{
	std::array<std::bitset<35>, 35> m{};
	// set the proper size
	for (int i = 0; i < cities; ++i) {
		m[i] |= ~0ULL << cities;
		m[i].set(i);
	}
	// fill the adjacency matrix
	long long x, y;
	for (; roads > 0; --roads) {
		std::cin >> x >> y;
		m[x - 1].set(y - 1);
		m[y - 1].set(x - 1);
	}

	// sort the array so the biggest is on the top
	std::sort(m.begin(), m.begin() + cities, bitset_compare);
	return m;
}

int rec(matrix const& m, size_t index, size_t max_index, int depth, int count, std::bitset<35> b = {})
{
	if (depth >= count) {
		return count;
	}

	for (; index < max_index; ++index) {
		// if OR == 1
		std::bitset<35> current_mask = b | m[index];
		if (current_mask.all()) {
			return depth;
		}

		// if the field brings nothing new, skip it
		if (current_mask == b) {
			continue;
		}

		// iterating only bigger indices so we don't permute
		int val = rec(m, index + 1, max_index, depth + 1, count, current_mask);
		if (val < count) {
			count = val;
		}
	}
	return count;
}

int find_minimum(int cities, int roads)
{
	matrix m = build_matrix(cities, roads);
	return rec(m, 0, cities, 1, cities);
}

int main()
{
	std::ios_base::sync_with_stdio(false);
	int cities, roads;
	while (true) {
		std::cin >> cities >> roads;
		if (cities == 0) {
			break;
		}
		std::cout << find_minimum(cities, roads) << '\n';
	}
	return 0;
}