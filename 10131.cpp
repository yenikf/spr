// 10131 - Is Bigger Smarter

#include <iostream>
#include <vector>
#include <algorithm>

using index = size_t;

struct Elephant
{
	index number;
	int weight;
	int iq;
};

bool compare_elephants(Elephant const& lhs, Elephant const& rhs)
{
	return (lhs.weight < rhs.weight || (lhs.weight == rhs.weight && lhs.iq > rhs.iq)) ? true : false;
}

std::vector<Elephant> read_elephants()
{
	std::vector<Elephant> elephants;
	index number = 1;
	int weight, iq;
	while (true) {
		std::cin >> weight >> iq >> std::ws;
		if (!std::cin) {
			break;
		}
		Elephant e{ number, weight, iq };
		elephants.push_back(e);
		++number;
	}
	return elephants;
}

std::vector<std::vector<index>> find_neighbours(std::vector<Elephant> const& e)
{
	std::vector<std::vector<index>> all_neighbours;
	for (index i = 0; i < e.size(); ++i) {
		std::vector<index> neighbours;
		for (index next = i + 1; next < e.size(); ++next) {
			if (e[next].weight > e[i].weight && e[next].iq < e[i].iq)
				neighbours.push_back(next);
		}
		all_neighbours.push_back(neighbours);
	}
	return all_neighbours;
}

int main()
{
	auto elephants = read_elephants();
	std::sort(elephants.begin(), elephants.end(), compare_elephants);
	auto neighbours = find_neighbours(elephants);

	std::vector<std::vector<index>> longest_path(elephants.size());
	index total_longest_path = 0;
	index longest_path_index = elephants.size() - 1;
	// iterating backwards
	for (int i = elephants.size() - 1; i >= 0; --i) {
		// no suitable neighbours means the longest sequence is this element by itself
		if (neighbours[i].empty()) {
			longest_path[i].push_back(i);
		} else {
			// find the longest sequence among previous neighbours
			index sum = 0;
			std::vector<index> tmp_path;
			for (index neighbour = 0; neighbour < neighbours[i].size(); ++neighbour) {
				if (longest_path[neighbours[i][neighbour]].size() > sum) {
					sum = longest_path[neighbours[i][neighbour]].size();
					tmp_path = longest_path[neighbours[i][neighbour]];
				}
			}
			// add the longest sequence to longest_path
			longest_path[i].push_back(i);
			longest_path[i].insert(longest_path[i].end(), tmp_path.begin(), tmp_path.end());
			// if it is the overall longest path, save it in advance
			if (longest_path[i].size() > total_longest_path) {
				total_longest_path = longest_path[i].size();
				longest_path_index = i;
			}
		}
	}

	// print the results
	std::cout << longest_path[longest_path_index].size() << '\n';
	for (auto x : longest_path[longest_path_index]) {
		std::cout << elephants[x].number << '\n';
	}

	return 0;
}