// 10044 - Erdos Numbers

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <set>
#include <unordered_map>
#include <queue>
#include <limits>

// translation layer between paper authors' names and graph indices
class StringIndex
{
public:
	size_t get_index(std::string const& s)
	{
		auto item = index_map.find(s);
		if (item != index_map.end()) {
			return item->second;
		} else {
			auto inserted = index_map.insert({ s, index++ });
			return inserted.first->second;
		}
	}

	bool contains_name(std::string const& s)
	{
		if (index_map.find(s) != index_map.end()) {
			return true;
		}
		return false;
	}

private:
	size_t index{};
	std::unordered_map<std::string, size_t> index_map;
};


class Node
{
public:
	void add_adjacent_node(size_t node)
	{
		adjacent_nodes.insert(node);
		return;
	}

	std::set<size_t> const& get_adjacent_nodes() const
	{
		return adjacent_nodes;
	}

	void set_erdos(size_t num)
	{
		if (num < erdos_number || erdos_number == 0) {
			erdos_number = num;
		}
		return;
	}

	size_t get_erdos() const
	{
		return erdos_number;
	}

private:
	std::set<size_t> adjacent_nodes;
	size_t erdos_number{};
};


class Graph
{
public:
	Graph()
	{
		si.get_index("Erdos, P.");
		nodes.resize(1);
		nodes[0].set_erdos(0);
	}

	void add_paper(std::vector<std::string> const& paper)
	{
		for (size_t i = 0; i < paper.size() - 1; ++i) {
			// adjust the vector size for new node
			if (si.get_index(paper[i]) + 1 > nodes.size()) {
			     nodes.resize(si.get_index(paper[i]) + 1);
			}
			for (size_t j = i + 1; j < paper.size(); ++j) {
				// adjust the vector size for new node
				if (si.get_index(paper[j]) + 1 > nodes.size()) {
					nodes.resize(si.get_index(paper[j]) + 1);
				}
				nodes[si.get_index(paper[i])].add_adjacent_node(si.get_index(paper[j]));
				nodes[si.get_index(paper[j])].add_adjacent_node(si.get_index(paper[i]));
			}
		}
		return;
	}

	// populate graph
	void generate_erdos_numbers()
	{
		// start with Erdos, P. in the queue and loop
		std::queue<size_t> q;
		q.push(0);
		std::set<size_t> processed_nodes;
		while (!q.empty()) {
			size_t index = q.front();
			q.pop();
			size_t current_erdos = nodes[index].get_erdos() + 1;
			std::set<size_t> const& adjacents = nodes[index].get_adjacent_nodes();
			for (size_t adjacent : adjacents) {
				// if this adjacent node isn't processed yet
				if (processed_nodes.find(adjacent) == processed_nodes.end()) {
					nodes[adjacent].set_erdos(current_erdos);
					q.push(adjacent);
				}
			}
			processed_nodes.insert(index);
		}
		return;
	}

	void print_erdos_number(std::ostream& os, std::string const& s)
	{
		// if the name is not present, Erdos number is infinity
		if (si.contains_name(s)) {
			size_t number = nodes[si.get_index(s)].get_erdos();
			if (number == 0) {
				os << "infinity";
			} else {
				os << std::to_string(number);
			}
		} else {
			os << "infinity";
		}
	}

	void print() const
	{
		for (size_t i = 0; i < nodes.size(); ++i) {
			auto adj = nodes[i].get_adjacent_nodes();
			std::cout << "node " << i << ": ";
			for (auto x : adj) {
				std::cout << x << "  ";
			}
			std::cout << '\n';
		}
		return;
	}

private:
	std::vector<Node> nodes;
	StringIndex si;
};


void scenario()
{
	size_t paper_count{};
	size_t querry_count{};
	std::cin >> paper_count >> querry_count >> std::ws;
	Graph graph;
	for (std::string line; paper_count > 0; --paper_count) {
		// get line of names for further parsing
		std::getline(std::cin, line, ':');
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::istringstream names(line);

		// read and reconstruct each name
		std::vector<std::string> paper_authors;
		std::string name, name_frac;
		for (; !names.eof(); ) {
			names >> std::ws;
			std::getline(names, name_frac, ',');
			name = name_frac + ',';
			std::getline(names, name_frac, ',');
			name += name_frac;
			paper_authors.push_back(name);
		}
		// interpret name pairs as graph nodes
		graph.add_paper(paper_authors);

	}
	// the algorithm
	graph.generate_erdos_numbers();

	// print the interested results
	for (std::string line; querry_count > 0; --querry_count) {
		std::getline(std::cin, line);
		std::cout << line << ' ';
		graph.print_erdos_number(std::cout, line);
		std::cout << '\n';
	}

	return;
}

int main()
{
	int n;
	std::cin >> n;
	for (int i = 1; i <= n; ++i) {
		std::cout << "Scenario " << i << '\n';
		scenario();
	}
	return 0;
}
