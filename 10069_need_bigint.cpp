// 10069 - Distinct Subsequences

#include <iostream>
#include <array>
#include <vector>
#include <map>
#include <string>

struct letters
{
	letters(std::string const& s)
		// reserve the size beforehand so we won't invalidate the iterators into
		: counts(s.size())
	{
		for (int i = 0; i < s.size(); ++i) {
			m.insert({ s[i], &(counts[i]) });
		}
	}

	bool is_first_letter(std::multimap<char, int *>::iterator iter)
	{
		return iter->second == &(counts[0]);
	}

	std::vector<int> counts;
	std::multimap<char, int *> m;
};

int solve()
{
	std::string text, word;
	std::cin >> std::ws;
	std::getline(std::cin, text);
	std::getline(std::cin, word);
	letters indexed(word);

	for (int i = 0; i < text.size(); ++i) {
		auto letter = indexed.m.equal_range(text[i]);
		while (letter.first != letter.second) {
			--(letter.second);
			// if it's first letter in the word
			int * letter_count = letter.second->second;
			if (indexed.is_first_letter(letter.second)) {
				// add one
				*letter_count += 1;
			} else {
				// or add the previous letter count
				*letter_count += *(letter_count - 1);
			}
		}
	}

	return indexed.counts.back();
}

int main()
{
	int tests;
	std::cin >> tests;
	while (tests--) {
		std::cout << solve() << '\n';
	}
	return 0;
}
