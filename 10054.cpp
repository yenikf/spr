// 10054 - The Necklace

#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <algorithm>
#include <numeric>

using colour = size_t;
using beadset = std::map<colour, std::map<colour, int>>;

bool is_connected(beadset const& colours)
{
	std::queue<colour> to_visit;
	std::set<colour> visited;
	// first color goes to the queue
	to_visit.push(colours.cbegin()->first);
	while (!to_visit.empty()) {
		colour visiting = to_visit.front();
		to_visit.pop();
		// if we already visited this node
		if (visited.find(visiting) != visited.end()) {
			continue;
		}
		// put all adjacent nodes to the queue
		std::for_each(colours.at(visiting).cbegin(), colours.at(visiting).cend(), [&to_visit](std::pair<colour, int> val) { to_visit.push(val.first); });
		// this node is taken care of
		visited.insert(visiting);
	}
	if (visited.size() != colours.size()) {
		return false;
	}
	return true;
}

std::vector<std::vector<colour>> make_cycles(beadset& colours)
{
	std::vector<std::vector<colour>> cycles;
	while (!colours.empty()) {
		std::vector<colour> cycle;
		// pick first color_present
		cycle.push_back(colours.cbegin()->first);
		while (true) {
			// previous node: pick from the end of cycle
			colour prev_colour = cycle.back();
			// next node: pick first edge
			colour next_colour = colours.at(prev_colour).begin()->first;
			// remove the edge
			colours[prev_colour][next_colour]--;
			colours[next_colour][prev_colour]--;
			// we need to remove the zeroed edges
			if (colours[prev_colour][next_colour] == 0) {
				colours[prev_colour].erase(next_colour);
				colours[next_colour].erase(prev_colour);
				if (colours[prev_colour].empty()) {
					colours.erase(prev_colour);
				}
				if (colours[next_colour].empty()) {
					colours.erase(next_colour);
				}
			}
			// if the next node is first node, end the loop
			if (cycle.front() == next_colour) {
				break;
			}
			// else add the next node to the cycle
			cycle.push_back(next_colour);
		}
		// save the cycle
		cycles.push_back(std::move(cycle));
	}
	return cycles;
}

std::vector<colour> make_full_cycle(std::vector<std::vector<colour>>& cycles)
{
	std::vector<colour> full_cycle(cycles.front());
	cycles.erase(cycles.begin());

	while (!cycles.empty()) {
	new_loop:
		for (size_t curr_cycle = 0; curr_cycle < cycles.size(); ++curr_cycle) {
			for (size_t i = 0; i < full_cycle.size(); ++i) {
				for (size_t j = 0; j < cycles[curr_cycle].size(); ++j) {
					if (full_cycle[i] == cycles[curr_cycle][j]) {
						// match found
						std::rotate(cycles[curr_cycle].begin(), cycles[curr_cycle].begin() + j, cycles[curr_cycle].end());
						full_cycle.insert(full_cycle.begin() + i, cycles[curr_cycle].begin(), cycles[curr_cycle].end());
						cycles.erase(cycles.begin() + curr_cycle);
						goto new_loop;
					}
				}
			}
		}
	}
	return full_cycle;
}

void print_cycle(std::vector<colour> const& full_cycle)
{
	colour first = full_cycle.front();
	std::cout << (first + 1) << ' ';
	for (colour i = 1; i < full_cycle.size(); ++i) {
		std::cout << (full_cycle[i] + 1) << '\n' << (full_cycle[i] + 1) << ' ';
	}
	std::cout << (first + 1) << '\n';
	return;
}

void test(int casenum)
{
	std::cout << "Case #" << casenum << '\n';

	// the graph adjacency matrix
	beadset colours{};

	// read input
	colour beads;
	std::cin >> beads;
	for (colour col1, col2, i = 0; i < beads; ++i) {
		std::cin >> col1 >> col2;
		colours[col1 - 1][col2 - 1]++;
		colours[col2 - 1][col1 - 1]++;
	}

	// every node must have an even number of edges
	for (auto const& row : colours) {
		if (std::accumulate(row.second.cbegin(), row.second.cend(), 0, [](int sum, std::pair<colour, int> elem) { return sum += elem.second; }) % 2 != 0) {
			std::cout << "some beads may be lost\n";
			return;
		}
	}

	// the graph must be connected
	if (!is_connected(colours)) {
		std::cout << "some beads may be lost\n";
		return;
	}

	//find the cycles
	auto cycles = make_cycles(colours);

	// merge the cycles
	auto full_cycle = make_full_cycle(cycles);

	// print the result
	print_cycle(full_cycle);

	return;
}

int main()
{
	std::ios_base::sync_with_stdio(false);
	int n, i = 0;
	std::cin >> n;
	while (i++ < n) {
		test(i);
		if (i != n) {
			std::cout << '\n';
		}
	}
	return 0;
}
