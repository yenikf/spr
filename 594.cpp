// 594 - One Little, Two Little, Three Little Endians

#include <iostream>

union u
{
	int32_t i;
	char c[4];
};

int main()
{
	for (int32_t i; std::cin >> i; ) {
		u j{ i };
		std::swap(j.c[0], j.c[3]);
		std::swap(j.c[1], j.c[2]);
		std::cout << i << " converts to " << j.i << '\n';
	}
	return 0;
}