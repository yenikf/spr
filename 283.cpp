// 283 - Compress

#include <iostream>
#include <sstream>
#include <string>
#include <array>
#include <algorithm>
#include <limits>

// 2^n - 1, for n = 1 to 6.
// Bigger values make no sense as there is about 57 chars to compress max.
// Zero at the beginning to match the exponent with the index
constexpr int group_size[] = { 0, 1, 3, 7, 15, 31, 63 };

// large enough default values
constexpr long long MAX_SUM = 100000000;
constexpr long long MAX_CODE = 15;

struct occurence
{
	long long sum{ MAX_SUM };
	long long codelength{ MAX_CODE };
};

std::array<int, 128> read_text(int lines)
{
	std::array<int, 128> letter_array{};

	// ignore the rest of the previous line
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	std::string line;
	while (lines-- > 0) {
		std::getline(std::cin, line);
		std::istringstream is(line);
		while (true) {
			unsigned char letter;
			is >> std::noskipws >> letter;
			if (!is) {
				break;
			}
			if (letter == '\n') {
				continue;
			}
			letter_array[letter]++;
		}
	}

	std::sort(letter_array.begin() + 1, letter_array.end(), std::greater<int>());
	return letter_array;
}

// from (not inclusive), to (inclusive)
long long sum_occurences(std::array<int, 128> const& letter_array, size_t from, size_t to)
{
	long long sum = 0;
	for (; to > from; --to) {
		sum += letter_array[to];
	}
	return sum;
}

void recurse(std::array<int, 128> const& letters, std::array<occurence, 128>& occ, size_t size, size_t position)
{
	for (int exp = 6; exp > 0; --exp) {
		// if the group_size is big make it fit
		size_t newindex = position + group_size[exp] > size ? size : position + group_size[exp];

		// set new code length - it is the old plus the exp
		long long newcodel = occ[position].codelength + exp;

		// sum the new block
		long long newsum = occ[position].sum + sum_occurences(letters, position, newindex) * newcodel;

		// if this is the last element
		if (newindex == size) {
			// if it has better sum value
			if (newsum < occ[newindex].sum) {
				occ[newindex].sum = newsum;
				occ[newindex].codelength = newcodel;
			}
			continue;
		// if the newsum has better both sum and codelength, set the values and continue recurring
		} else if (newsum <= occ[newindex].sum && newcodel <= occ[newindex].codelength) {
			occ[newindex].sum = newsum;
			occ[newindex].codelength = newcodel;
		// if the newsum has worse both values, stop the recurse
		} else if (newsum >= occ[newindex].sum && newcodel >= occ[newindex].codelength) {
			continue;
		// if we can't decide, we have to decide differently
		} else {
			// if it is the one before last element
			if (newindex + 1 == size) {
				long long lastsum = occ[newindex].sum + sum_occurences(letters, newindex, newindex + 1) * occ[newindex].codelength;
				if (lastsum < occ[newindex + 1].sum) {
					occ[newindex + 1].sum = lastsum;
					occ[newindex + 1].codelength = occ[newindex].codelength;
				}
			}
			// run the recurse
			recurse(letters, occ, size, newindex);

			occ[newindex].sum = newsum;
			occ[newindex].codelength = newcodel;
			// run the recurse again with new values
		}

		// if this is the one before the last element we can reuse the last bit combo
		if (newindex + 1 == size) {
			long long lastsum = newsum + sum_occurences(letters, newindex, newindex + 1) * newcodel;
			if (lastsum < occ[newindex + 1].sum) {
				occ[newindex + 1].sum = lastsum;
				occ[newindex + 1].codelength = newcodel;
			}
		}

		// if it makes sense to recurse
		if (newindex < size) {
			recurse(letters, occ, size, newindex);
		}
	}
	return;
}

void run_test()
{
	int lines;
	std::cin >> lines;

	auto letters = read_text(lines);
	size_t size = std::count_if(letters.begin(), letters.end(), [](int i) { return i != 0; });

	std::array<occurence, 128> occ;
	occ[0].sum = 0, occ[0].codelength = 0;

	recurse(letters, occ, size, 0);

	std::cout << occ[size].sum << '\n';

	return;
}

int main()
{
	int n;
	std::cin >> n;

	while (n-- > 0) {
		run_test();
	}
	return 0;
}
