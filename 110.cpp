// 110 - Meta-Loopless Sorts

#include <iostream>
#include <cstdio>
#include <string>
#include <utility>

char * chars = "abcdefgh";

void tab(int depth)
{
	for (int i = 0; i < depth; ++i) {
		printf("  ");
	}
	return;
}

void writeln(int const depth, std::string const& s)
{
	tab(depth);
	printf("writeln(%c", s[0]);
	for (int i = 1; i < s.size(); ++i) {
		printf(",%c", s[i]);
	}
	printf(")\n");
	return;
}

void rec(int depth, int maxdepth, std::string s)
{
	if (depth == maxdepth) {
		writeln(depth, s);
		return;
	}
	s = chars[depth] + s;
	for (int i = 0; i <= depth; ++i) {
		if (i != 0) {
			tab(depth);
			printf("else ");
		}
		if (i != depth) {
			tab(depth);
			printf("if %c < %c then\n", s[i], s[i + 1]);
		}
		rec(depth + 1, maxdepth, s);
		std::swap(s[i], s[i + 1]);
	}
	return;
}

void pascal(int m)
{
	printf("program sort(input,output);\nvar\na");
	for (int i = 1; i < m; ++i) {
		printf(",%c", chars[i]);
	}
	printf(" : integer;\nbegin\n  readln(a");
	for (int i = 1; i < m; ++i) {
		printf(",%c", chars[i]);
	}
	printf(");\n");
	rec(1, m, "a");
	printf("end.\n");
	return;
}

int main()
{
	std::ios_base::sync_with_stdio(false);
	int n;
	std::cin >> n;
	static std::string empty;
	std::getline(std::cin, empty);
	for (; n > 0; --n) {
		int m;
		std::cin >> m;
		pascal(m);
		if (n != 1) {
			printf("\n");
		}
	}
	return 0;
}