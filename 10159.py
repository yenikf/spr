﻿# 10159 - Star

import sys

class Star:
    def __init__(self, nums):
        if len(nums) != 12:
            raise ValueError
        # max numbers of each line, dictionary of 'line => number' pairs.
        self.numbers = {}
        for x in range(12):
            self.numbers[chr(ord('A') + x)] = nums[x]
        # lines that has the number as max, list of tuples of 'number => (line1, line2)'.
        self.lines = [()] * 10
        for key, value in self.numbers.items():
            self.lines[self.numbers[key]] += key,
        # lines that cross at each intersection
        self.intersections = [()] * 48
        for lineoffset in range(12):
            line = chr(ord('A') + lineoffset)
            for index in Star.indicesOf(line):
                self.intersections[index] += line,
        # list of all the cells and their number
        self.cells = [-1] * 48

    def fillCells(self):
        '''Fills all cells with maximum possible number and checks if the maximum number
        present in each line is right.'''
        for same_num_lines in self.lines:
            for line in same_num_lines:
                maxnum_present = False
                for index in Star.indicesOf(line):
                    if self.cells[index] == -1:
                        self.cells[index] = self.numbers[line]
                        maxnum_present = True
                    if self.cells[index] == self.numbers[line]:
                        maxnum_present = True
                if maxnum_present == False:
                    return False
        return True

    def findIntersections(self, lines, higherlines):
        '''Find all intersections that cover at least one line given.'''
        relevant_intersections = []
        for intersection in self.intersections:
            include = False
            reject = False
            for line in intersection:
                if line in lines:
                    include = True
                elif line not in higherlines:
                    reject = True
            if not reject and include:
                if intersection not in relevant_intersections:
                    relevant_intersections.append(intersection)
        return relevant_intersections

    def filterIntersections(self, lines, intersections):
        '''Filter intersections so that only the most occurent set is processed later.'''
        if not intersections:
            return []
        if len(intersections) == 1:
            return intersections
        # rank the intersections according to number of occurences.
        in_ranked = { i : 0 for i in intersections }
        for i in intersections:
            for line in lines:
                if line in i:
                    in_ranked[i] = in_ranked.get(i, 0) + 1
        # pick only intersections with the most lines
        in_selected = sorted(in_ranked, reverse = True, key = lambda x : in_ranked[x])
        in_selected = [ i for i in in_selected if in_ranked[i] == in_ranked[in_selected[0]]]
        return in_selected

    def findBestIntersections(self, lines, intersections):
        '''Find the smallest set of intersections covering all lines.'''
        in_filtered = self.filterIntersections(lines, intersections)
        in_recurse = intersections.copy()
##        print('\tfiltered {}'.format(in_filtered))
        intersections_return = []
        for i in in_filtered:
            newlines = lines - set(i)
            if not newlines:
##                print('\t\tShortcut return i: {}'.format(i))
                return [i]

            in_recurse = intersections.copy()
            in_recurse.remove(i)
            ret_inters = self.findBestIntersections(newlines, in_recurse)
##            print('\t\ti: {}\n\t\tret_inters: {}\n\t\treturn: {}'.format(i, ret_inters, intersections_return))
            if not intersections_return or len(ret_inters) + 1 < len(intersections_return):
                intersections_return = ret_inters + [i]
        return intersections_return

    def largestSum(self):
        '''Counts the largest possible sum - it's simply a sum of fillCells() output.'''
        return sum(self.cells)
    
    def smallestSum(self):
        '''Counts the smallest possible sum.'''
        minsum = 0;
        higherlines = set()
        for maxnum in range(9,0,-1):
            # skip the empty numbers
            if not self.lines[maxnum]:
                continue
            actuallines = set(self.lines[maxnum])
##            print ('Intersections at {}:\n\tlines {}\n\thigherlines {}.'.format(maxnum, actuallines, tuple(higherlines)))
            intersections = self.findIntersections(actuallines, higherlines);
##            print('\tfound {}'.format(intersections))
            if len(intersections) > 1:
                intersections = self.findBestIntersections(actuallines, intersections)
            minsum += maxnum * len(intersections)
##            print('\tselected {}, adding {}, total {}.'.format(intersections, maxnum * len(intersections), minsum))
            higherlines.update(*intersections)
            actuallines.difference_update(*intersections)
        return minsum

    def intersectionAt(self, cell):
        '''Lines that intersect given cell.'''
        return self.intersections[cell]

    def indicesOf(line):
        '''Cell indices that each line intersect.'''
        if (line == 'A'):
            return set(range(4, 15))
        elif (line == 'B'):
            return set(range(15, 24))
        elif (line == 'C'):
            return set(range(24, 33))
        elif (line == 'D'):
            return set(range(33, 44))
        elif (line == 'E'):
            return set((0, 1, 2, 7, 8, 16, 17, 24, 25, 33, 34))
        elif (line == 'F'):
            return set((3, 9, 10, 18, 19, 26, 27, 35, 36))
        elif (line == 'G'):
            return set((11, 12, 20, 21, 28, 29, 37, 38, 44))
        elif (line == 'H'):
            return set((13, 14, 22, 23, 30, 31, 39, 40, 45, 46, 47))
        elif (line == 'I'):
            return set((4, 5, 15, 16, 25, 26, 36, 37, 44, 45, 47))
        elif (line == 'J'):
            return set((6, 7, 17, 18, 27, 28, 38, 39, 46))
        elif (line == 'K'):
            return set((1, 8, 9, 19, 20, 29, 30, 40, 41))
        elif (line == 'L'):
            return set((0, 2, 3, 10, 11, 21, 22, 31, 32, 42, 43))
        else:
            return set()


for line in sys.stdin:
    numbers = tuple(int(x) for x in line.split())
    s = Star(numbers)
    if s.fillCells():
        print (s.smallestSum(), s.largestSum())
    else:
        print('NO SOLUTION')
