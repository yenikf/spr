// 10196 – Check The Check

#include <iostream>
#include <array>
#include <cctype>
#include <cstdlib>

using board = std::array<std::array<int, 12>, 12> ;

struct king
{
	int x, y;
};

bool read_chessboard(board& cb, king& w, king& b)
{
	int blank = 0;
	for (int y = 2; y < 10; ++y) {
		for (int x = 2, ch; x < 10 && (ch = std::cin.get()) != '\n'; ++x) {
			cb[y][x] = ch;
			if (ch == '.') {
				++blank;
			} else if (ch == 'k') {
				b.x = x;
				b.y = y;
			} else if (ch == 'K') {
				w.x = x;
				w.y = y;
			}
		}
		std::cin >> std::ws;
	}
	return blank == 64 ? false : true;
}

bool check_square(board const& cb, king const& k, int f, int y, int x)
{
	return (cb[k.y + y][k.x + x] == f) ? true : false;
}

template <typename F, typename I>
bool check_range(board const& cb, king const& orig, F f1, F f2, I move)
{
	king k = orig;
	move(k);
	while (cb[k.y][k.x] == '.') {	// iterate away the space
		move(k);
	}
	if (cb[k.y][k.x] == f1 || cb[k.y][k.x] == f2) {
		return true;
	}
	return false;
}

template <typename F>
bool iterate_positions(board const& b, king const& k, int pawn, F fig)
{
	if (check_square(b, k, fig('P'), pawn, -1)		// pawns
		|| check_square(b, k, fig('P'), pawn, 1)
		|| check_square(b, k, fig('N'), -2, -1)		// knights
		|| check_square(b, k, fig('N'), -2, 1)
		|| check_square(b, k, fig('N'), -1, -2)
		|| check_square(b, k, fig('N'), -1, 2)
		|| check_square(b, k, fig('N'), 1, -2)
		|| check_square(b, k, fig('N'), 1, 2)
		|| check_square(b, k, fig('N'), 2, -1)
		|| check_square(b, k, fig('N'), 2, 1)
		|| check_range(b, k, fig('R'), fig('Q'), [](king& k) { k.x -= 1; })	// rooks and queens
		|| check_range(b, k, fig('R'), fig('Q'), [](king& k) { k.x += 1; })
		|| check_range(b, k, fig('R'), fig('Q'), [](king& k) { k.y -= 1; })
		|| check_range(b, k, fig('R'), fig('Q'), [](king& k) { k.y += 1; })
		|| check_range(b, k, fig('B'), fig('Q'), [](king& k) { k.x -= 1; k.y -= 1; })	// bishops and queens
		|| check_range(b, k, fig('B'), fig('Q'), [](king& k) { k.x -= 1; k.y += 1; })
		|| check_range(b, k, fig('B'), fig('Q'), [](king& k) { k.x += 1; k.y -= 1; })
		|| check_range(b, k, fig('B'), fig('Q'), [](king& k) { k.x += 1; k.y += 1; })) {
		return true;
	}
	return false;
}

bool find_check()
{
	board chessboard{};
	king wk, bk;
	if (!read_chessboard(chessboard, wk, bk)) {
		return false;
	}
	static int game_count{ 1 };
	std::cout << "Game #" << game_count++ << ": ";
	if (iterate_positions(chessboard, bk, +1, [](int x) { return x; })) {
		std::cout << "black";
	} else if (iterate_positions(chessboard, wk, -1, [](int x) { return std::tolower(x); })) {
		std::cout << "white";
	} else {
		std::cout << "no";
	}
	std::cout << " king is in check.\n";
	return true;
}

int main()
{
	std::ios_base::sync_with_stdio(false);
	while (find_check()) {}
	return 0;
}