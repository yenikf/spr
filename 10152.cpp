// 10152 - ShellSort

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>

void test_case()
{
	int length;
	std::cin >> length;
	std::vector<std::string> before;
	std::vector<std::string> after;
	std::string name;
	std::getline (std::cin, name);		// get rid of the newline

	for (int i = 0; i < length; ++i) {
		std::getline(std::cin, name);
		before.push_back(name);
	}
	for (int i = 0; i < length; ++i) {
		std::getline(std::cin, name);
		after.push_back(name);
	}

	// we don't care to know the moved elements, we just need to know how many there are, they are always on the top of 'after'.
	int diff_count{};
	for (auto iter = after.rbegin(), orig = before.rbegin(); orig != before.rend(); ) {
		if (*iter == *orig) {
			++orig, ++iter;
		} else {
			++orig;
			++diff_count;
		}
	}

	std::vector<std::string> diff;
	std::copy_n(after.cbegin(), diff_count, std::back_inserter(diff));
	std::reverse(diff.begin(), diff.end());

	std::copy(diff.cbegin(), diff.cend(), std::ostream_iterator<std::string>(std::cout, "\n"));
	std::cout << "\n";
	return;
}

int main()
{
	std::ios_base::sync_with_stdio(false);
	int n;
	std::cin >> n;
	for (; n > 0; --n) {
		test_case();
	}
	return 0;
}