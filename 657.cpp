// 657 - The Die is Cast

#include <iostream>
#include <vector>
#include <algorithm>

struct point
{
	point()
		: point(0, 0, ' ', false)
	{}

	point(int x_, int y_, char c_, bool v_)
		: x(x_), y(y_), content(c_), visited(v_)
	{}

	int x{};
	int y{};
	char content{};
	bool visited{};
};

using picture = std::vector<std::vector<point>>;

bool is_dice(point const& p)
{
	return p.content == '*' || p.content == 'X';
}

bool is_number(point const& p)
{
	return p.content == 'X';
}

picture read_input(int x, int y)
{
	picture input;
	input.reserve(y);
	for (int i = 0; i < y; ++i) {
		std::vector<point> v;
		char c{};
		v.reserve(x);
		for (int j = 0; j < x; ++j) {
			std::cin >> c;
			point p{ j, i, c, false };
			v.push_back(p);
		}
		input.push_back(v);
	}
	return input;
}

void print_input(picture const& v)
{
	for (auto const& line : v) {
		for (auto const p : line) {
			std::cout << p.content;
		}
		std::cout << '\n';
	}
	return;
}

template <typename F>
std::vector<point> get_object_from(picture& c, point start_point, F contains)
{
	std::vector<point> die;
	die.push_back(start_point);
	for (int idx = 0; idx < die.size(); ++idx) {
		point p = die[idx];
		// look up
		if (p.y - 1 >= 0 && contains(c[p.y - 1][p.x]) && c[p.y - 1][p.x].visited == false) {
			c[p.y - 1][p.x].visited = true;
			die.push_back(c[p.y - 1][p.x]);
		}
		// look down
		if (p.y + 1 < c.size() && contains(c[p.y + 1][p.x]) && c[p.y + 1][p.x].visited == false) {
			c[p.y + 1][p.x].visited = true;
			die.push_back(c[p.y + 1][p.x]);
		}
		// look left
		if (p.x - 1 >= 0 && contains(c[p.y][p.x - 1]) && c[p.y][p.x - 1].visited == false) {
			c[p.y][p.x - 1].visited = true;
			die.push_back(c[p.y][p.x - 1]);
		}
		// look right
		if (p.x + 1 < c[0].size() && contains(c[p.y][p.x + 1]) && c[p.y][p.x + 1].visited == false) {
			c[p.y][p.x + 1].visited = true;
			die.push_back(c[p.y][p.x + 1]);
		}
	}
	// reset the visited flag for later
	for (auto& p : die) {
		p.visited = false;
	}
	return die;
}

template <typename F>
picture get_object(picture& c, F contains)
{
	picture dice;
	for (int y = 0; y < c.size(); ++y) {
		for (int x = 0; x < c[0].size(); ++x) {
			if (contains(c[y][x]) && c[y][x].visited == false) {
				dice.push_back(get_object_from(c, c[y][x], contains));
			}
		}
	}
	return dice;
}

picture translate_die(std::vector<point> const& die)
{
	auto y_limits = std::minmax_element(die.begin(), die.end(), [](point const& lhs, point const& rhs) { return lhs.y < rhs.y; });
	int min_y = y_limits.first->y;
	int max_y = y_limits.second->y;
	auto x_limits = std::minmax_element(die.begin(), die.end(), [](point const& lhs, point const& rhs) { return lhs.x < rhs.x; });
	int min_x = x_limits.first->x;
	int max_x = x_limits.second->x;
	picture pic(max_y - min_y + 1);
	for (auto& line : pic) {
		line.resize(max_x - min_x + 1);
	}
	for (auto const& p : die) {
		pic[p.y - min_y][p.x - min_x] = { p.x - min_x, p.y - min_y, p.content, false };
	}
	return pic;
}

void solve(int width, int height)
{
	auto input = read_input(width, height);
	auto dice = get_object(input, is_dice);
	std::vector<picture> dice_pictures;
	for (auto const& die : dice) {
		dice_pictures.push_back(translate_die(die));
	}
	std::vector<size_t> dice_numbers;
	for (auto& die : dice_pictures) {
		dice_numbers.push_back(get_object(die, is_number).size());
	}
	std::sort(dice_numbers.begin(), dice_numbers.end());
	for (int i = 0; i < dice_numbers.size(); ++i) {
		if (i != 0) {
			std::cout << ' ';
		}
		std::cout << dice_numbers[i];
	}
	std::cout << "\n\n";
	return;
}

int main()
{
	int x{}, y{}, count{};
	while (true) {
		std::cin >> x >> y;
		if (x == 0 || y == 0) {
			break;
		}
		std::cout << "Throw " << ++count << '\n';
		solve(x, y);
	}
	return 0;
}