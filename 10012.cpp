// 10012 - How big is it?

#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

class Permutation
{
public:
	Permutation(int sz);

	double minimal_sum();
private:
	void sqrt_lut();
	double count_sum();
	void permute(double& d);

	std::vector<double> circles;					// actual circle sizes
	std::vector<int> available, permut;				// since we need to read all the members of the stack at once we use std::vector for the permut
	std::vector<std::vector<double>> square_roots;	// square root lookup table
};

Permutation::Permutation(int sz)
{
	circles.resize(sz);
	// read the circles
	for (auto& circle : circles) {
		std::cin >> circle;
	}

	// compute the square roots
	sqrt_lut();

	// put the indices into pool of available indices
	available.reserve(sz);
	for (int i = 0; i < sz; ++i) {
		available.push_back(i);
	}
	permut.reserve(sz);
}

double Permutation::minimal_sum()
{
	double minsum = 10e10;
	permute(minsum);
	return minsum;
}

void Permutation::sqrt_lut()
{
	square_roots.resize(circles.size());
	for (auto & vec : square_roots) {
		vec.resize(circles.size());
	}
	for (int i = 0; i < circles.size(); ++i) {
		for (int j = i + 1; j < circles.size(); ++j) {
			square_roots[i][j] = std::sqrt(circles[i] * circles[j]);
			square_roots[j][i] = square_roots[i][j];
		}
	}
	return;
}

double Permutation::count_sum()
{
	std::vector<double> distance(circles.size());

	for (size_t i = 1; i < circles.size(); ++i) {
		for (int j = 0; j < i; ++j) {
			double last_distance = distance[j] + 2 * square_roots[permut[j]][permut[i]];
			// pick the maximum distance so the circles don't overlap
			distance[i] = last_distance > distance[i] ? last_distance : distance[i];
		}
	}

	double begin_overlap = 0, end_overlap = 0;
	for (size_t i = 0; i < circles.size(); ++i) {
		if (circles[permut[i]] - distance[i] > begin_overlap) {
			begin_overlap = circles[permut[i]] - distance[i];
		}
		if (circles[permut[i]] + distance[i] - distance.back() > end_overlap) {
			end_overlap = circles[permut[i]] + distance[i] - distance.back();
		}
	}

	distance.back() += begin_overlap + end_overlap;
	return distance.back();
}

void Permutation::permute(double& d)
{
	// if the permutation contains all members
	if (available.empty()) {
		// count the max
		double tmpval = count_sum();
		// if the new sum is smaller, use it
		if (tmpval < d) {
			d = tmpval;
		}
		return;
	}
	// permutation algorithm using the stack
	for (int i = 0; i < available.size(); ++i) {
		// move a member from the available ones to the permutation
		permut.push_back(available[i]);
		available.erase(available.begin() + i);
		// recurse on the remaining members
		permute(d);
		// move the member out
		available.insert(available.begin() + i, permut.back());
		permut.pop_back();
		// and choose the next one
	}
	return;
}
	
int main()
{
	std::ios_base::sync_with_stdio(false);
	std::cout << std::fixed << std::setprecision(3);
	int n;
	std::cin >> n;
	for (; n > 0; --n) {
		int how_many;
		std::cin >> how_many;
		Permutation p(how_many);

		std::cout << p.minimal_sum() << '\n';
	}
	return 0;
}