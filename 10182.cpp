// 10278 - Bee Maja

#include <iostream>
#include <utility>

struct maya
{
	maya(int circle);
	int x{};
	int sloped_y{};
};

maya::maya(int circle)
	: x{ circle - 1 }, sloped_y{ 1 }
{}

std::pair<int, int> find_circle(int n)
// returns n's circle and position in the circle (in interval [zero ... '% == 0')).
{
	--n;
	int circle = 1;
	int circle_sum = 0;
	int next_circle_size = 6;
	while (n > circle_sum + next_circle_size) {
		circle_sum += next_circle_size;
		next_circle_size += 6;
		++circle;
	}
	return std::make_pair(circle, n - circle_sum);
}

void translate(int n)
{
	if (n == 1) {
		std::cout << "0 0\n";
		return;
	}
	auto willy = find_circle(n);
	int circle = willy.first;
	int position_in_circle = willy.second;
	int current_step = 1;
	// direction: 0 to 5 clockwise
	int direction = circle == 1 ? 5 : 4;

	maya m(circle);
	while (current_step < position_in_circle) {
		// move
		switch (direction % 6) {
			case 0:
				--m.sloped_y;
				break;
			case 1:
				++m.x;
				--m.sloped_y;
				break;
			case 2:
				++m.x;
				break;
			case 3:
				++m.sloped_y;
				break;
			case 4:
				--m.x;
				++m.sloped_y;
				break;
			case 5:
				--m.x;
				break;
		}
		++current_step;
		// change the direction
		if (current_step % circle == 0) {
			direction = (direction + 1) % 6;
		}
	}
	std::cout << m.x << ' ' << m.sloped_y << '\n';
	return;
}

int main()
{
	std::ios_base::sync_with_stdio(false);
	int n;
	while (std::cin >> n) {
		translate(n);
	}
	return 0;
}