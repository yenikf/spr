// 10013 - Super long sums

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>

void find_sum()
{
	int length;
	std::cin >> length;
	std::vector<int> v(1);		// 'one' for carry at the most significant figure

	for (int i, j; length > 0; --length) {	// read the numbers and sum them
		std::cin >> i >> j;
		v.push_back(i + j);
	}

	for (auto it = v.rbegin(); it != v.rend(); ++it) {	// propagate the carry
		if (*it > 9) {
			*it -= 10;
			*(it + 1) += 1;
		}
	}

	if (*v.begin() == 0) {		// remove leading zero, if any
		v.erase(v.begin());
	}
	std::copy(v.cbegin(), v.cend(), std::ostream_iterator<int>(std::cout));
	std::cout << '\n';
	return;
}

int main()
{
	std::ios_base::sync_with_stdio(false);
	int n;
	std::cin >> n;
	for (; n > 0; --n) {
		static std::string empty;
		std::getline(std::cin, empty);
		find_sum();
		if (n > 1) {
			std::cout << '\n';
		}
	}
	return 0;
}