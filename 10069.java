// 10069 - Distinct Subsequences

import java.io.*;
import java.math.BigInteger;

public class spr10069 {

	private static BigInteger solve(String text, String word) {
		
		// counts the occurences
		BigInteger counts[] = new BigInteger[word.length()];
		// don't forget to create the objects!
		for (int i = 0; i < counts.length; ++i) {
			counts[i] = BigInteger.ZERO;
		}
		
		// for each letter of text
		for (int i = 0; i < text.length(); ++i) {
			// test each letter in the word
			for (int j = word.length() - 1; j >= 0; --j) {
				if (text.charAt(i) == word.charAt(j)) {
					if (j == 0) {
						// if it's the first letter in word, add one
						counts[0] = counts[0].add(BigInteger.ONE);
					} else {
						// else add the count of previous letter
						counts[j] = counts[j].add(counts[j - 1]);
					}
				}
			}
		}
		 return counts[word.length() - 1];
	}
	
	public static void main(String[] args) {
		// read the number of tests
		int tests = 0;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
			String s = br.readLine();
			tests = Integer.parseInt(s);
			while(tests-- != 0) {
				String text = "", word = "";
				text = br.readLine();
				word = br.readLine();
				System.out.println(solve(text, word));
			}
		} catch (IOException | NumberFormatException e) {
			System.out.println("Bad input error 1");
		}
		
		// solve each test
		return;
	}
}
