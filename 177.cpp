// 177 - Paper Folding

#include <iostream>
#include <array>
#include <vector>
#include <algorithm>

enum head
{
	north, west, south, east
};

head& operator++(head& h)
{
	h = static_cast<head>((h + 1) % 4);
	return h;
}

head& operator--(head& h)
{
	h = static_cast<head>((h + 3) % 4);		// +3 = -1 + 4; the negative modulo is faulty!
	return h;
}

enum class side
{
	left = 1, right
};

static std::array<side, (1 << 13)> folds;	// 2 ** 13 max_size

struct draw_element
{
	draw_element() = default;
	draw_element(int tx, int ty, char tch)
		:x(tx), y(ty), ch(tch)
	{};

	int x{};
	int y{};
	char ch{};
};

bool operator<(draw_element const& lhs, draw_element const& rhs)	// so we can sort them later
{
	if ((lhs.y < rhs.y) || ((lhs.y == rhs.y) && (lhs.x < rhs.x))) {
		return true;
	}
	return false;
}

int arrlen(int input)
{
	return 1 << input;
}

void paper_fold(int min, int max, int depth, side s)
{
	int mid = (min + max) / 2;
	if (--depth > 0) {
		paper_fold(min, mid, depth, side::left);
		paper_fold(mid, max, depth, side::right);
	}
	folds[mid] = s;
	return;
}

void draw_fold(int num_folds)
{
	std::vector<draw_element> elems(num_folds);
	head h = head::east;
	elems[0] = draw_element(0, 0, '_');
	int coord_x = 1, coord_y = 0;
	for (int i = 1; i < num_folds; ++i) {
		if (folds[i] == side::left) {		// rotate the view
			++h;
		} else {
			--h;
		}
		switch (h) {		// move the position
			case north:
				elems[i] = { coord_x, coord_y, '|' };
				--coord_y;
				break;
			case west:
				--coord_x;
				elems[i] = { coord_x, coord_y, '_' };
				--coord_x;
				break;
			case south:
				++coord_y;
				elems[i] = { coord_x, coord_y, '|' };
				break;
			case east:
				++coord_x;
				elems[i] = { coord_x, coord_y, '_' };
				++coord_x;
				break;
		}
	}
	std::sort(elems.begin(), elems.end());		// sort the elements, find their limits
	int min_x = std::min_element(elems.cbegin(), elems.cend(), [](draw_element const& lhs, draw_element const& rhs) { return lhs.x < rhs.x; })->x;
	int min_y = std::min_element(elems.cbegin(), elems.cend(), [](draw_element const& lhs, draw_element const& rhs) { return lhs.y < rhs.y; })->y;
	for (int index = 0, x = min_x, y = min_y; index < elems.size(); ++index) {		// draw the dragon curve
		if (y < elems[index].y) {
			x = min_x;
			++y;
			std::cout << '\n';
		}
		while (x < elems[index].x) {
			++x;
			std::cout << ' ';
		}
		std::cout << elems[index].ch;
		++x;
	}
	std::cout << '\n';
	return;
}

int main()
{
	std::ios_base::sync_with_stdio(false);
	int n;
	while (std::cin >> n) {
		if (n == 0) {
			break;
		}
		paper_fold(0, arrlen(n), n, side::left);
		draw_fold(arrlen(n));
		std::cout << "^\n";
	}
	return 0;
}
