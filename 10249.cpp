// 10249 - The Grand Dinner

#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
#include <numeric>

struct Table
{
	Table(int n, int s)
		: number(n), seats(s), available(s)
	{}

	int number;
	int seats;
	int available;
};

struct Team
{
	Team(int n, int m)
		: number(n), members(m), unseated(m)
	{}

	bool sits_at(int const table) const
	{
		return tables.find(table) != tables.cend();
	}

	void sit(int const table)
	{
		tables.insert(table);
		return;
	}

	void print_tables() const
	{
		for (auto table : tables) {
			std::cout << table << ' ';
		}
		std::cout << '\n';
	}

	int number;
	int members;
	int unseated;
private:
	std::set<int> tables;
};

std::vector<Table> read_tables(int num)
{
	std::vector<Table> tables;
	tables.reserve(num);
	for (int i = 1, x; i <= num; ++i) {
		std::cin >> x;
		tables.emplace_back(i, x);
	}
	return tables;
}

std::vector<Team> read_teams(int num)
{
	std::vector<Team> teams;
	teams.reserve(num);
	for (int i = 1, x; i <= num; ++i) {
		std::cin >> x;
		teams.emplace_back(i, x);
	}
	return teams;
}

void solve(int t, int u)
{
	auto teams = read_teams(t);
	auto tables = read_tables(u);

	while (true) {
		auto teams_end = std::partition(teams.begin(), teams.end(), [](Team const& x) {return x.unseated > 0; });
		// if there is no team left, everyone is seated
		if (teams.begin() == teams_end) {
			std::sort(teams.begin(), teams.end(), [](Team const& lhs, Team const& rhs) {return lhs.number < rhs.number; });
			std::cout << "1\n";
			for (auto const& team : teams) {
				team.print_tables();
			}
			return;
		}
		auto tables_end = std::partition(tables.begin(), tables.end(), [](Table const& x) {return x.available > 0; });
		int total_teams = std::accumulate(teams.cbegin(), teams.cend(), 0, [](int x, Team const& y) {return x + y.unseated; });
		int total_tables = std::accumulate(tables.cbegin(), tables.cend(), 0, [](int x, Table const& y) {return x + y.available; });
		// if there is more team members than empty seats, end with an error
		if (total_teams > total_tables) {
			std::cout << "0\n";
			return;
		}
		std::sort(teams.begin(), teams_end, [](Team const& lhs, Team const& rhs) { return lhs.unseated > rhs.unseated; });
		// if the biggest team has more members than there is tables, end with an error
		if (teams[0].unseated > (tables_end - tables.begin())) {
			std::cout << "0\n";
			return;
		}
		std::sort(tables.begin(), tables_end, [](Table const& lhs, Table const& rhs) { return lhs.available > rhs.available; });

		// take the biggest team and seat it at the largest tables available, each member at different table
		for (int table_place = 0; teams[0].unseated > 0;) {
			// if there is no more place at the table (next table is empty) end with an error
			if (tables[table_place].available == 0) {
				std::cout << "0\n";
				return;
			}
			// if another team member sits at this table already
			if (teams[0].sits_at(tables[table_place].number)) {
				continue;
			}
			teams[0].sit(tables[table_place].number);
			teams[0].unseated -= 1;
			tables[table_place].available -= 1;
			++table_place;
		}
	}
	return;
}

int main()
{
	std::ios_base::sync_with_stdio(false);
	int teams, tables;
	while (true) {
		std::cin >> teams >> tables;
		if (teams == 0) {
			break;
		}
		solve(teams, tables);
	}
	return 0;
}